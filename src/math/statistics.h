#ifndef STATISTICS_H
#define STATISTICS_H

#include <QVector>
#include <QtMath>
#include <QHash>
#include <QList>

/*!
 * \brief Třída Statistics
 *
 * Tato třída obsahuje základní často používané matematické předpisy ze statistiky.
 */
class Statistics {

public:
    /*!
     * \brief max Metoda najdve největší hodnotu v zadaném vektoru.
     * \param set Výběr hodnot.
     * \return Největší hodnot zadaného výběru.
     */
    template <class T>
    static T max(const QVector<T> &set);

    /*!
     * \brief indexOfMax Metoda vrátí index, na kterém se nalézá největší prvek vektoru. Pokud
     *                   je nejvyšších hodnot více, bude vrácen prvek s nejmenším indexem.
     * \param set Výběr hodnot.
     * \return Index největší hodnoty zadaného výběru.
     */
    template <class T>
    static int indexOfMax(const QVector<T> &set);

    /*!
     * \brief max Metoda najdve nejmenší hodnotu v zadaném vektoru.
     * \param set Výběr hodnot.
     * \return Nejmenší hodnot zadaného výběru.
     */
    template <class T>
    static T min(const QVector<T> &set);

    /*!
     * \brief indexOfMax Metoda vrátí index, na kterém se nalézá nejmenší prvek vektoru. Pokud
     *                   je nejmenších hodnot více, bude vrácen prvek s nejmenším indexem.
     * \param set Výběr hodnot.
     * \return Index nejmenší hodnoty zadaného výběru.
     */
    template <class T>
    static int indexOfMin(const QVector<T> &set);

    /*!
     * \brief standardDeviation Metoda vypočítá směrodatnou odchylku hodnot zadaného vektoru.
     * \param set Výběr hodnot.
     * \return Směrodatná odchylka zadaného výběru.
     */
    template <class T>
    static double standardDeviation(const QVector<T> &set);

    /*!
     * \brief modus Metoda vypočítá modus hodnota zaného vektoru.
     * \param set Výběr hodnot.
     * \return Modus zadaného výběru.
     */
    template <class T>
    static T modus(const QVector<T> &set);

    /*!
     * \brief median Metoda vypočítá medián hodnot zadaného vektoru.
     * \param set Výběr hodnot.
     * \return Medián zadaného výběru.
     */
    template <class T>
    static double median(const QVector<T> &set);

    /*!
     * \brief variance Metoda vypočítá rozptyl hodnot zadaného vektoru, kdy je uvažována stejná
     *                 pravděpodobnost všech prvků výběru.
     * \param set Výběr hodnot.
     * \return Rozptyl hodnot výběru.
     */
    template <class T>
    static double variance(const QVector<T> &set);

    /*!
     * \brief arithmeticMean Metoda vypočítá aritmetický průměr hodnot zadaného vektoru.
     * \param set Výběr hodnot.
     * \return Aritmetický průměr hodnot zadaného vektoru.
     */
    template <class T>
    static double arithmeticMean(const QVector<T> &set);

    /*!
     * \brief geometricMean Metoda vypočítá geometrický průměr hodnot zadaného vektoru.
     * \param set Výběr hodnot.
     * \return Geometrický průměr hodnot zadaného vektoru.
     */
    template <class T>
    static double geometricMean(const QVector<T> &set);
};

template<class T>
T Statistics::max(const QVector<T> &set) {
    if (set.isEmpty())
        return T();

    return set[Statistics::indexOfMax<T>(set)];
}

template<class T>
int Statistics::indexOfMax(const QVector<T> &set) {
    if (set.isEmpty())
        return -1;

    T highest = set[0];
    int indexOfHighest = 0;

    for (int i = 1; i < set.size(); i++) {
        if (set[i] > highest) {
            indexOfHighest = i;
            highest = set[i];
        }
    }

    return indexOfHighest;
}

template<class T>
T Statistics::min(const QVector<T> &set) {
    if (set.isEmpty())
        return T();

    return set[Statistics::indexOfMin<T>(set)];
}

template<class T>
int Statistics::indexOfMin(const QVector<T> &set) {
    if (set.isEmpty())
        return -1;

    T smalles = set[0];
    int indexOfSmallest = 0;

    for (int i = 1; i < set.size(); i++) {
        if (set[i] < smalles) {
            indexOfSmallest = i;
            smalles = set[i];
        }
    }

    return indexOfSmallest;
}

template<class T>
double Statistics::standardDeviation(const QVector<T> &set) {
    return sqrt(Statistics::variance<T>(set));
}

template<class T>
T Statistics::modus(const QVector<T> &set) {
    QHash<T, int> occurrences;

    if (set.isEmpty())
        return T();

    for (const T& i : set)
        occurrences[i]++;

    QList<int> occurVals = occurrences.values();
    int indexOfHighest = Statistics::indexOfMax<T>(occurVals.toVector());

    return occurrences.keys()[indexOfHighest];
}

template<class T>
double Statistics::median(const QVector<T> &set) {
    QVector<T> sortedSet = set;

    if (set.isEmpty())
        return T();

    std::sort(sortedSet.begin(), sortedSet.end());

    if (sortedSet.size() % 2 == 0) {
        QVector<T> mid = { sortedSet[(sortedSet.size() / 2) - 1], sortedSet[sortedSet.size() / 2] };

        return Statistics::arithmeticMean<T>(mid);
    }
    else {
        int medianIndex = static_cast<int>(qCeil(sortedSet.size() / 2));

        return sortedSet[medianIndex];
    }
}

template<class T>
double Statistics::variance(const QVector<T> &set) {
    double sigma = 0;
    double mean = Statistics::arithmeticMean<T>(set);

    if (set.isEmpty())
        return T();

    for (const T &i : set)
        sigma += qPow(static_cast<double>(i) - mean, 2.0);

    return sigma / static_cast<double>(set.size());
}

template<class T>
double Statistics::arithmeticMean(const QVector<T> &set) {
    double mean = 0;

    if (set.isEmpty())
        return T();

    for (const T &i : set)
        mean += i;

    return mean / static_cast<double>(set.size());
}

template<class T>
double Statistics::geometricMean(const QVector<T> &set) {
    if (set.isEmpty())
        return T();

    double multiplication = set[0];
    for (int i = 1; i < set.size(); i++)
        multiplication *= set[i];

    return qPow(multiplication, 1 / static_cast<double>(set.size()));
}

#endif
