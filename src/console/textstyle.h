#ifndef TEXTSTYLE_H
#define TEXTSTYLE_H

#include <QObject>
#include <QString>
#include <QVector>
#include <QVectorIterator>

/*!
 * \brief Třída TextStyle
 *
 * Třída představuje možnost stylování textu vypisováného na konzolový výstup aplikací. Metody této
 * třídy jednoduše přidávají příznaky pro stylování (obarvovaní popředí a pozadí a přidávání dekorací
 * jako je ztučnění a podtržení textu).
 */
class TextStyle {

public:
    /*!
     * \brief Enum ForegroudColors
     *
     * Výčet možný barev popředí textu.
     */
    enum ForegroudColors {
        DefaultFore = 0,        //!< Výchozí barva popředí (můj výmysl).
        BlackFore   = 30,       //!< Černá barva popředí.
        RedFore     = 31,       //!< Červený barva popředí.
        GreenFore   = 32,       //!< Zelená barva popředí.
        YellowFore  = 33,       //!< Žlutá barva popředí.
        BlueFore    = 34,       //!< Modrá barva popředí.
        MagentaFore = 35,       //!< "Purpurová" barva popředí.
        CyanFore    = 36,       //!< "Světle modrá" barva popředí.
        WhiteFore   = 37        //!< Bílá barva popředí.
    };

    /*!
     * \brief Enum BackgroudColors
     *
     * Výčet možných barev pozadí textu.
     */
    enum BackgroudColors {
        DefaultBack = 0,        //!< Výchozí barva pozadí (můj výmysl).
        BlackBack   = 40,       //!< Černá barva pozadí.
        RedBack     = 41,       //!< Červená barva pozadí.
        GreenBack   = 42,       //!< Zelená barva pozadí.
        YellowBack  = 43,       //!< Žlutá barva pozadí.
        BlueBack    = 44,       //!< Modrá barva pozadí.
        MagentaBack = 45,       //!< "Purpurová" barva pozadí.
        CyanBack    = 46,       //!< "Světle modrá" barva pozadí.
        WhiteBack   = 47        //!< Bílá barva pozadí.
    };

    /*!
     * \brief Enum Decorations
     *
     * Tento enumerický typ obsahuje možné dekorace pro text. Navíc, zcela nelogicky, obsahuje příznak
     * reset, který způsobuje navrácení veškerých předem definovaných stylů do výchozích hodnot OS.
     * Pozn.: Pokud k jednolivým hodnotám dekorací přičtete hodnotu 20, znamená to ukončení dané
     * dekorace. Této skutečnosti se využívá v metodě TextStyle::decoreText(...).
     */
    enum Decorations {
        Bold            = 1,        //!< Text bude vykreslný tučně.
        Underline       = 4,        //!< Text bude vykreslený podtržený.
        Inverse         = 7,        //!< U textu bude budou prohozeny barvy popředí a pozadí.

        Reset           = 0         //!< Příznak, který který všechny předchozí styly převede do původního stavu.
    };

    /*!
     * \brief TextStyle Konstruktor třídy, který je využívaný pouze jako přepravka (název je odvozen podle
     *                  stejnojmenného návrhového vzoru -- cradle).
     * \param foregraund Barva popředí textu.
     * \param background Barva pozadí textu.
     * \param decorations Vektor použitých dekorací textu.
     */
    explicit TextStyle(const TextStyle::ForegroudColors &foregraund = ForegroudColors::DefaultFore,
                            const TextStyle::BackgroudColors &background = BackgroudColors::DefaultBack,
                            const QVector<TextStyle::Decorations> &decorations = { });

    /*!
     * \brief foreground Metoda vrátí předvolenou hodnotu atributu barvy popředí textu.
     * \return Předvolená barva popředí textu.
     */
    TextStyle::ForegroudColors foreground() const;

    /*!
     * \brief backgroud Metoda vrátí předvolenou hodnotu atributu barvy pozadí textu.
     * \return Předvolená barva pozadí textu.
     */
    TextStyle::BackgroudColors background() const;

    /*!
     * \brief decorations Metoda vrátí seznam předvolených dekorací textu.
     * \return Vektor předvolených dekorací textu.
     */
    QVector<TextStyle::Decorations> decorations() const;

    /*!
     * \brief colorizeText Metoda do zadaného řetězce přidá uvozující skupiny znaků, které konzolové
     *                     rozhraní chápe jako obarvení textu. Jako ukončující příznak je používán
     *                     Decorations::Reset, který způsobí ukončení veškerého dekorování textu.
     * \param text Text, který bude obarven.
     * \param foreground Barva popředí textu.
     * \param background Barva pozadí textu.
     * \return Obarvený text.
     */
    static QString colorizeText(const QString &text, const TextStyle::ForegroudColors &foreground = ForegroudColors::DefaultFore,
                                    const TextStyle::BackgroudColors &background = BackgroudColors::DefaultBack);

    /*!
     * \brief decoreText Metoda do zadaného řetězce přidá uvozující skupiny znaků, které konzolové
     *                   rozhraní chápue jako dekorace textu. Ukončující znaky jsou párové, takže
     *                   ukončení dekorací nijak neovlivní jiné použité dekorace nebo barvy.
     * \param text Dekorovaný text.
     * \param decorations Vektor dekorací pro text.
     * \return Dekorovaný text.
     */
    static QString decoreText(const QString &text, const QVector<TextStyle::Decorations> &decorations = {});

    /*!
     * \brief styleText Metoda provede stylizaci textu podle zadané instance třídy TextStyle. Teto
     *                  metoda je z hlediska syntaktické estetičnosti výstupního textu ideální.
     * \param text Text, který bude stylizován.
     * \param style Styl, který bude použit pro stylizaci textu.
     * \return Stylizovaný text.
     */
    static QString styleText(const QString &text, const TextStyle &style);

private:
    TextStyle::ForegroudColors m_foreground;            //!< Barva popředí textu.
    TextStyle::BackgroudColors m_background;            //!< Barva pozadí textu.
    QVector<TextStyle::Decorations> m_decorations;      //!< Dekorace textu.

    /*!
     * \brief stylingMark Podoba příznaku, kterému rozumí konzolový výstup. Místo sekvence %1 budou vloženy
     *                    jednotlivé hodnoty stylů textu. Jednotlivé hodnoty stylů jsou oddělovány pomocí znaku ';'.
     */
    static constexpr auto stylingMark = "\u001b[%1m";

    /*!
     * \brief creteStylingMark Metoda pomocí řetězce TextStyle::stylingMark a zadaných stylů vytvoří
     *                         patřičný stylovací příznak pro konzolový výstup.
     * \param styles Vektor stylů, které budou zahrnuty do výsledného příznaku.
     * \return Výsledný stylovací příznak.
     */
    static QString createStylingMark(const QVector<int> &styles);

    /*!
     * \brief createResetMark Specializovaná metoda, která vytvoří příznak pro ukončení veškerého
     *                        stylování textu.
     * \return Příznak pro ukončení stylování textu.
     */
    static QString createResetMark();

    /*!
     * \brief creteStyleList Metoda vytvoří seznam navolených stylů podle zadané instance třídy TextStyle.
     * \param style Instance třídy, ze které bude vytvořen seznam stylů.
     * \return Vektor stylů.
     */
    static QVector<int> createStyleList(const TextStyle& style);
};

Q_DECLARE_METATYPE(TextStyle::ForegroudColors);
Q_DECLARE_METATYPE(TextStyle::BackgroudColors);
Q_DECLARE_METATYPE(TextStyle::Decorations);
Q_DECLARE_METATYPE(QVector<TextStyle::Decorations>);

#endif
