#include "console.h"

void Console::write(const QString &message, const TextStyle &style) {
    if (message.isEmpty())
        return;

    std::cout << TextStyle::styleText(message, style).toStdString();
}

void Console::writeLine(const QString &message, const TextStyle &style) {
    if (message.isEmpty()) {
        std::cout << std::endl;
        return;
    }

    std::cout << TextStyle::styleText(message, style).toStdString() << std::endl;
}

void Console::writeSuccess(const QString &successMessage) {
    if (successMessage.isEmpty())
        return;

    TextStyle bannerStyle(TextStyle::GreenFore, TextStyle::DefaultBack, { TextStyle::Bold });
    TextStyle messageStyle(TextStyle::GreenFore, TextStyle::DefaultBack, { });

    Console::write(Console::successMessageBanner, bannerStyle);
    Console::write(successMessage, messageStyle);
    Console::writeLine();
}

void Console::writeError(const QString &errorMessage) {
    if (errorMessage.isEmpty())
        return;

    TextStyle bannerStyle(TextStyle::RedFore, TextStyle::DefaultBack, { TextStyle::Bold });
    TextStyle messageStyle(TextStyle::RedFore, TextStyle::DefaultBack, { });

    Console::write(Console::errorMessageBanner, bannerStyle);
    Console::write(errorMessage, messageStyle);
    Console::writeLine();
}

void Console::writeWarning(const QString &warningMessage) {
    if (warningMessage.isEmpty())
        return;

    TextStyle bannerStyle(TextStyle::YellowFore, TextStyle::DefaultBack, { TextStyle::Bold });
    TextStyle messageStyle(TextStyle::YellowFore, TextStyle::DefaultBack, { });

    Console::write(Console::warningMessageBanner, bannerStyle);
    Console::write(warningMessage, messageStyle);
    Console::writeLine();
}

void Console::writeNotice(const QString &noticeWarning) {
    if (noticeWarning.isEmpty())
        return;

    TextStyle bannerStyle(TextStyle::BlueFore, TextStyle::DefaultBack, { TextStyle::Bold });
    TextStyle messageStyle(TextStyle::BlueFore, TextStyle::DefaultBack, { });

    Console::write(Console::noticeMessageBanner, bannerStyle);
    Console::write(noticeWarning, messageStyle);
    Console::writeLine();
}

void Console::writeDebugInfo(const QString &debugMessage, bool bold) {
    if (debugMessage.isEmpty())
        return;

    QVector<TextStyle::Decorations> decorations;
    if (bold)
        decorations << TextStyle::Bold;

    TextStyle messageStyle(TextStyle::WhiteFore, TextStyle::MagentaBack, decorations);

    Console::writeLine(debugMessage, messageStyle);
}

QString Console::readLine() {
    std::string input;

    std::cin >> input;
    return QString::fromStdString(input);
}

bool Console::writeQuestion(const QString &question, const TextStyle &style) {
    if (question.isEmpty())
        return false;

    QString toWrite = QString("%1 (%2/%3): ")
                        .arg(question)
                        .arg(static_cast<char>(UserAnswers::Agreement))
                        .arg(static_cast<char>(UserAnswers::Disagreement));

    Console::write(toWrite, style);

    QString answer = Console::readLine();
    QRegularExpression re(QString() += UserAnswers::Agreement);
    QRegularExpressionMatch match = re.match(answer);

    return match.hasMatch();
}

bool Console::writeConfirm(const QString &statement, const TextStyle &style) {
    if (statement.isEmpty())
        return false;

    QString toWrite = QString("%1 (%2/%3): ")
                        .arg(statement)
                        .arg(static_cast<char>(UserAnswers::Ok))
                        .arg(static_cast<char>(UserAnswers::Disagreement));

    Console::write(toWrite, style);

    QString answer = Console::readLine();
    QRegularExpression re(QString() += UserAnswers::Ok);
    QRegularExpressionMatch match = re.match(answer);

    return match.hasMatch();
}

