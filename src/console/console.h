#ifndef CONSOLE_H
#define CONSOLE_H

#include <QString>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <iostream>

#include "textstyle.h"

/*!
 * \brief Třída Console
 *
 * Třída obsahuje kolekci statických metod, které dovolují snadnou práci s konzolovým rozhraním
 * mých aplikací. Framework Qt podle všeho neobsahuje přímou možnost těchto operací, tak jsem si
 * je implementoval sám...
 */
class Console {

public:
    /*!
     * \brief write Metoda do výstupního proudu stdout vypíše zadaný text. Pokud je vstupní text prázdný,
     *              neprovede se žádná akce.
     * \param style Stylování vypsaného textu.
     * \param message Zpráva, která má být vytištěna do konzole.
     */
    static void write(const QString &message, const TextStyle &style = TextStyle());

    /*!
     * \brief writeLine Metoda do výstupního proudu stdou vypíše zadaný text, který automaticky ukončí znakem
     *                  endl. Pokud je vstupní text prázdný, pouze odřádkuje.
     * \param style Stylování vypsaného textu.
     * \param message Zpráva, která má být vytištěna do konzole.
     */
    static void writeLine(const QString &message = "", const TextStyle &style = TextStyle());

    /*!
     * \brief writeSuccess Metoda do výstupní proudu zapíše zadaný text ve formátu SUCCESS_MESSAGE_BANNER
     *                     + successMessage. Text prefixu bude tučný a oba řetězce budou napsány zeleným písmem.
     * \param successMessage
     */
    static void writeSuccess(const QString &successMessage);

    /*!
     * \brief writeError Metoda do výstupního proudu zapíše chybové hlášení ve formátu ERROR_MESSAGE_PREFIX
     *                   + errorMessage. Text prefixu bude tučný a oba řetězce budou napsány červeným písmem.
     * \param errorMessage Obsah chybového hlášení.
     */
    static void writeError(const QString &errorMessage);

    /*!
     * \brief writeWarning Metoda do výstupního proudu zapíše varovné hlášení ve formátu WARNING_MESSAGE_BANNER
     *                     + warningMessage. Text prefixu bude tučný a oba řetězce budou napsány žlutým písmem.
     * \param warningMessage Obsah varovného hlášení.
     */
    static void writeWarning(const QString &warningMessage);

    /*!
     * \brief writeNotice Metoda do výstupního proudu zapíše upozornění ve formátu NOTICE_MESSAGE_BANNER
     *                    + noticeWarning. Text prefixu bude tučný a oba řetězce budou napsány modrým písmem.
     * \param noticeWarning Obsah poznámky.
     */
    static void writeNotice(const QString &noticeWarning);

    /*!
     * \brief writeDebugInfo Metoda do výstupního proudu zapíše ladící informace (prostě text z parametru),
     *                       který bude vykreslen purpurovou barvou.
     * \param debugMessage Text, který bude vypsán do konzole.
     * \param bold Pokud bude hodnota parametru true, text bude vypsán tučně.
     */
    static void writeDebugInfo(const QString &debugMessage, bool bold = false);

    /*!
     * \brief readLine Metoda ze vstupního proudu přečte jeden řádek, který vrátí ve formě objektu třídy QString.
     *                 Volání této metody je blokující.
     * \return Jeden řádek přečtený ze vstupního proudu.
     */
    static QString readLine();

    /*!
     * \brief writeQuestion Metoda vypíše zadanou otázku do výstupního proudu pomocí metody Console::print a přečte
     *                      odpověď uživatele pomocí metody Console::readLine. Pokud odpověď odpovídá znaku
     *                      UserAnswers::Agreement, metoda vrací hodnotu true, tj. uživatel se zprávou souhlasí, jinak false.
     *                      Pokud je vstupní otázka prázdná, metoda neprovede žádnou akci a ihned vrátí hodnotu false.
     * \param question Otázka, která je uživateli vypsána do výstupního proudu stdout.
     * \param style Stylování vypsaného textu.
     * \return True, pokud odpověď uživatele odpovídá znaku UserAnswers::Agreement, jinak false.
     */
    static bool writeQuestion(const QString &question, const TextStyle &style = TextStyle());

    /*!
     * \brief writeConfirm Metoda vypíše zadavý výrok do výstupního proudu pomocí metody Console::print a přečte
     *                     odpovď uživatele pomocí metody Console::readLine. Pokud odpověď odpovídá znaku
     *                     UserAnswers::Ok, metoda vrací hodnotu true, tj. uživatel bere zprávu na vědomí, jinak false.
     *                     Pokud je vstupní tvrzení prázdným řetězcem, metoda neprovede žádnou akci a ihned vrátí hodnotu false.
     * \param statement Výrok, který je uživateli vypsán do výstupního proudu stdout.
     * \param style Stylování vypsaného textu.
     * \return True, pokud uživatel bere zprávu na vědomí, jinak false.
     */
    static bool writeConfirm(const QString &statement, const TextStyle &style = TextStyle());

private:
    /*!
     * \brief Enum UserAnswers
     *
     * Enum, který obsahuje možné odpovědi uživatele.
     */
    enum UserAnswers {
        Agreement    = 'y',         //!< Znak, který je od uživatele chápán jako souhlas.
        Disagreement = 'n',         //!< Znak, který je od uživatele chápán jakon nesouhlas.
        Ok           = 'o'          //!< Znak, který je od uživatele chávám jako pochopení tvrzení (Ok).
    };

    /*!
     * \brief successMessageBanner Prefix, který je vypsán před samotnou zprávu při volání metody Console::writeSuccess(const QString
     *                             &successMessage).
     */
    static constexpr auto successMessageBanner = "Ok: ";

    /*!
     * \brief errorMessageBanner Prefix, který je vypsán před samotnou zprávu při volání metody Console::writeError(const QString
     *                           &errorMessage).
     */
    static constexpr auto errorMessageBanner = "Chyba: ";

    /*!
     * \brief warningMessageBanner Prefix, který je vypsán před samotnou zprávu při volání metody Console::writeWarning(const QString
     *                             &warningMessage).
     */
    static constexpr auto warningMessageBanner = "Varovani: ";

    /*!
     * \brief noticeMessageBanner Prefix, který je vypsán před samotnou zprávu při volání metody Console::writeNotice(const QString
     *                            &noticeMessage).
     */
    static constexpr auto noticeMessageBanner = "Poznamka: ";
};

#endif
