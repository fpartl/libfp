#include "textstyle.h"

TextStyle::TextStyle(const TextStyle::ForegroudColors &foregraund, const TextStyle::BackgroudColors &background,
                        const QVector<TextStyle::Decorations> &decorations) {
    m_foreground = foregraund;
    m_background = background;
    m_decorations = decorations;
}

TextStyle::ForegroudColors TextStyle::foreground() const {
    return m_foreground;
}

TextStyle::BackgroudColors TextStyle::background() const {
    return m_background;
}

QVector<TextStyle::Decorations> TextStyle::decorations() const {
    return m_decorations;
}

QString TextStyle::colorizeText(const QString &text, const TextStyle::ForegroudColors &foreground,
                                    const TextStyle::BackgroudColors &background) {
    QVector<int> styles = createStyleList(TextStyle(foreground, background));

    if (text.isEmpty() || styles.isEmpty())
        return text;

    return QString("%1%2%3")
                .arg(createStylingMark(styles))
                .arg(text)
                .arg(createResetMark());
}

QString TextStyle::decoreText(const QString &text, const QVector<TextStyle::Decorations> &decorations) {
    if (text.isEmpty() || decorations.isEmpty())
        return text;

    QVector<int> styles = createStyleList(TextStyle(ForegroudColors::DefaultFore,
                                            BackgroudColors::DefaultBack, decorations));

    QVector<int> endStyles;
    for (int i : styles)
        endStyles.prepend(i);

    return QString("%1%2%3")
                .arg(createStylingMark(styles))
                .arg(text)
                .arg(createStylingMark(endStyles));
}

QString TextStyle::styleText(const QString &text, const TextStyle &style) {
    QVector<int> styles = TextStyle::createStyleList(style);

    if (text.isEmpty() || styles.isEmpty())
        return text;

    return QString("%1%2%3")
                .arg(createStylingMark(styles))
                .arg(text)
                .arg(createResetMark());
}

QString TextStyle::createStylingMark(const QVector<int> &styles) {
    if (QString(TextStyle::stylingMark).isEmpty()) // při podmíněném překladu může být tímto stylováví zakázáno.
        return QString();

    QString styleString = "";

    if (styles.isEmpty())
        return QString();

    QVectorIterator<int> i(styles);
    while (i.hasNext()) {
        styleString += QString::number(i.next());

        if (i.hasNext())
            styleString += ';';
    }

    return QString(TextStyle::stylingMark).arg(styleString);
}

QString TextStyle::createResetMark() {
    return TextStyle::createStylingMark({ Decorations::Reset });
}

QVector<int> TextStyle::createStyleList(const TextStyle &style) {
    QVector<int> styles;

    if (style.foreground() != ForegroudColors::DefaultFore)
        styles << style.foreground();

    if (style.background() != BackgroudColors::DefaultBack)
        styles << style.background();

    /*
     * Ukázalo se, že tahle deklarace proměnné decorations je nutná! Jinak std::copy dělá totální mr... no.
     *
     * Třeba ten getter decorations dělá hlubokou kopii objektu. Takže při každém volání vlastně dostanu jinou
     * kopii toho co chci. Tím pádem se logicky kopíruje i to, co je mezi začátkem první kopie a koncem druhé
     * kopie -- balast. Zní to logicky a s jednotnou "kopií" to funguje.
     */
    QVector<TextStyle::Decorations> decorations = style.decorations();
    std::copy(decorations.begin(), decorations.end(), std::back_inserter(styles));

    return styles;
}
