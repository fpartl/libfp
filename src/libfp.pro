#-------------------------------------------------
#
# Project created by QtCreator 2019-09-06T11:53:34
#
#-------------------------------------------------

QT       -= gui

TARGET = libfp
TEMPLATE = lib
CONFIG += staticlib

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    console/console.cpp \
    console/textstyle.cpp \
    threading/workerrunner.cpp \
    threading/workerthread.cpp

HEADERS += \
    console/console.h \
    data/vectorprinter.h \
    math/statistics.h \
    structures/cyclicbuffer.h \
    patterns/eventproducer.h \
    threading/sharedqueue.h \
    console/textstyle.h \
    threading/workerrunner.h \
    threading/workerthread.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
