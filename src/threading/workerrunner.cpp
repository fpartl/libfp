#include "workerrunner.h"
#include "workerthread.h"

WorkerRunner::WorkerRunner(QObject *parent) : QObject(parent) { }

bool WorkerRunner::hasRunningWorkers() const {
    return !m_workers.isEmpty();
}

WorkerRunner::~WorkerRunner() {
    stopAllWorkers();
}

void WorkerRunner::stopAllWorkers() {
    for (WorkerThread *poor : m_workers) {
        poor->quit();

        if (!poor->wait(WorkerRunner::maxWaitTime))
            poor->terminate();
    }
}

void WorkerRunner::workerFinished() {
    WorkerThread *thread = reinterpret_cast<WorkerThread *>(QObject::sender());

    if (!thread || !m_workers.removeOne(thread)) {
        Console::writeError("WorkerRunner: Snažím se odstranit vlákno, které není evidováno.");
        return;
    }

    if (m_workers.isEmpty()) {
        emit allWorkersFinished();
        allWorkersDone();
    }
}

void WorkerRunner::runWorker(WorkerThread *worker, bool deleteLater) {
    if (!worker) return;

    m_workers.append(worker);

    worker->registerObserver(this);
    QObject::connect(worker, &WorkerThread::finished, this, &WorkerRunner::workerFinished);

    if (deleteLater)
        QObject::connect(worker, &WorkerThread::finished, worker, &WorkerThread::deleteLater);

    worker->start();
}

void WorkerRunner::handleError(const QString &message) {
    Console::writeError(message);
}

void WorkerRunner::handleWarning(const QString &message) {
    Console::writeWarning(message);
}

void WorkerRunner::handleNotice(const QString &message) {
    Console::writeNotice(message);
}
