#include "workerthread.h"

WorkerThread::WorkerThread(QObject *parent) : QThread(parent) { }

bool WorkerThread::isReady() {
    return true;
}

void WorkerThread::connectObserver(WorkerRunner *handler) {
    QObject::connect(this, &WorkerThread::warning, handler, &WorkerRunner::handleWarning);
    QObject::connect(this, &WorkerThread::error, handler, &WorkerRunner::handleError);
    QObject::connect(this, &WorkerThread::notice, handler, &WorkerRunner::handleNotice);
}

void WorkerThread::disConnectObserver(WorkerRunner *handler) {
    QObject::disconnect(this, &WorkerThread::warning, handler, &WorkerRunner::handleWarning);
    QObject::disconnect(this, &WorkerThread::error, handler, &WorkerRunner::handleError);
    QObject::disconnect(this, &WorkerThread::notice, handler, &WorkerRunner::handleNotice);
}
