#ifndef PARAMERWORKER_H
#define PARAMERWORKER_H

#include <QObject>
#include <QThread>

#include "../patterns/eventproducer.h"
#include "workerrunner.h"

/*!
 * \brief Třída ParamerThread
 *
 * Přestavuje obecné pracovní vlákno aplikace, které dokáže emitovat události
 * error, warning a notice, na což reagují pozorovatelé typu WorkerRunner.
 */
class WorkerThread : public QThread, public EventProducer<WorkerRunner> {
    Q_OBJECT

public:
    /*!
     * \brief ParamerThread Konstruktor třídy.
     * \param parent Ukazatel na rodiče objektu, který lze dle notace Qt využít k automatické destrukci objektu.
     */
    explicit WorkerThread(QObject *parent = nullptr);

    /*!
     * \brief isReady Metoda, jejíž účelem je kontrola, zda je pracovní vlákno připraveno k výkonu
     *                své činnosti. Je žádoucí, aby toto metoda byla v odděděných třídách
     * \return True, pokud je vlákno připraveno ke spuštění, jinak false.
     */
    bool isReady();

private:
    /*!
     * \brief connectObserver Metoda, která provede připojení observera (pozorovatele) k pracovnímu vláknu.
     * \param handler Ukazatel na připojovaného pozorovatele.
     */
    void connectObserver(WorkerRunner *handler) override;

    /*!
     * \brief disConnectObserver Metoda, která provede odpojení observer (pozorovatele) od pracovního vlákna.
     * \param handler Ukazatel na odpojovaného pozorovotela.
     */
    void disConnectObserver(WorkerRunner *handler) override;

signals:
    /*!
     * \brief error Signál, který informuje o závažné chybě v praconím vlákně.
     * \param message Konkrétní obsah chybového hlášení.
     */
    void error(const QString& message);

    /*!
     * \brief warning Signál, který nese varovnou zprávu.
     * \param message Obsah varovné zprávy.
     */
    void warning(const QString& message);

    /*!
     * \brief notice Signál, který nese obecnou upomínku.
     * \param message Obsah upomínky.
     */
    void notice(const QString& message);
};

#endif
