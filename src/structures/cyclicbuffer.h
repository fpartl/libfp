#ifndef CYCLICBUFFER_H
#define CYCLICBUFFER_H

#include <QObject>
#include <QVector>
#include <QtMath>

#include "../console/console.h"

/*!
 * \brief Třída CyclicBuffer
 *
 * Třída představuje obecný dynamicky alokovaný cyklický buffer. U bufferu lze zadat počáteční
 * velikost bufferu, hodnotu delta (po násobcích jakého čísla bude buffer realokován) a zda je
 * vůbec žádoucí buffer realokovat. Buffer je šablonovaný, nicméně využívá kontejneru QVector<T>,
 * který vyžaduje, aby třídy objektů v něm uchovávané měly defaultní konstruktor. Tuto vlastnost
 * bohužel tato třídy dědí a může tím pádem za těchto podmínek způsobovat obludné chybové výpisy.
 */
template <class T>
class CyclicBuffer : public QObject {

public:
    /*!
     * \brief CyclicBuffer Konstruktor třídy. Pomocí jeho parametrů lze blíže specifikovat počáteční
     *                     velikost a krok při případné realokaci. Tím lze velice markantně optimalizovat
     *                     paměťovou i časovou náročnost kruhového bufferu v různých aplikacích. Realokaci
     *                     je možné i kompletně vypnout. Při následném vkládání příliš velkého množství
     *                     dat do bufferu bude operace zakázána a patřičné metody budou vracet hodnoty false.
     * \param bufferSize Počáteční velikost bufferu, která má výchozí hodnotu DEFAULT_BUFFER_SIZE.
     * \param allowReallock Příznak, který říká, zda je povolena nebo zakázána realokace cyklického bufferu.
     * \param delta Po násobcích jakého čísla bude buffer realokován. Výchozí hodnota je DEFAULT_DELTA_VALUE.
     * \param parent Ukazatel na rodiče objektu, který lze dle notace Qt použít k automatickému
     *               uvolnění dynamicky alokovaného objektu této třídy.
     */
    explicit CyclicBuffer(int bufferSize = defaultBufferSize, bool allowReallock = true,
                                int delta = defaultDeltaValue, QObject *parent = nullptr);

    /*!
     * \brief write Metoda do cyklického bufferu zapíše jednu položku. Při zakázané realokaci a nedostatku
     *              volného místa v bufferu metoda vrací hodnotu false.
     * \param item Položka vložená do cycklického bufferu.
     * \return True, pokud byl zápis úspěšný, jinak false.
     */
    bool write(const T &item);

    /*!
     * \brief write Metoda, pomocí které lze zapsat data určená libovolným ukazelem do paměti. Při zakázané
     *              realokaci a nedostatku volného místa v bufferu metoda vrací hodnotu false.
     * \param data Ukazatel na vkládaná data.
     * \param size Počet vkládaných položek.
     * \return True, pokud bylo vložení provedeno úspěšně, jinak false.
     */
    bool write(const T *data, int size);

    /*!
     * \brief write Přetížená metoda. Zapíše do interního bufferu celý vektor dat. Při zakázané realokaci
     *              a nedostatku volného místa v bufferu metoda vrací hodnotu false.
     * \param data Data určená k zápisu do kruhového bufferu.
     * \return True, pokud byl zápis úspěšný, jinak false.
     */
    bool write(const QVector<T> &data);

    /*!
     * \brief isBuffered Metoda zkontroluje, zda buffer obsahuje alepospoň estSize položek.
     * \param estSize Počet požadavaných položek v interním bufferu.
     * \return True, pokud cyklický buffer obsahuje alespoň estSize položek, jinak false.
     */
    bool isBuffered(int estSize);

    /*!
     * \brief takeData Metoda provede extrakci count položek z interního bufferu, které vrátí
     *                 jako vektor. Pokud buffer tento počet položek neobsahuje, metoda vrátí
     *                 vektor všech dostupných položek.
     * \param size Počet extrahovaných položek z interního bufferu.
     * \return Vektor extrahovaných položek.
     */
    QVector<T> take(int size);

    /*!
     * \brief take Přetížená metoda. Provede extrakci count položek z interního bufferu, které
     *             vrátí jako vektor. Pokud buffer tento počet položek neobsahuje, metoda vrátí
     *             vektor všech dostupných položek. Nakonec se "začátek bufferu" posune jen
     *             count - dataOverlap hodnot, čímž se realizuje možný překryv při extrakci dat.
     * \param size Počet prvků, které budou extrahovány.
     * \param dataOverlap Posun "začátku bufferu".
     * \return Vektor extrahovaných položek.
     */
    QVector<T> take(int size, int dataOverlap);

    /*!
     * \brief exstractAll Přetížená metoda. Provede extrakci všech položek z interního bufferu, které vrátí
     *                    jako vektor.
     * \return Vektor odebraných položek.
     */
    QVector<T> takeAll();

    /*!
     * \brief clear Metoda vyprázdní interní buffer.
     */
    void clear();

    /*!
     * \brief isEmpty Metoda zjistí, zda je interní buffer prázný.
     * \return True, pokud je interní buffer prázdný, jinak false.
     */
    bool isEmpty();

    /*!
     * \brief size Metoda vrací počet prvků v bufferu.
     * \return Počet prvků v bufferu.
     */
    int count();

    /*!
     * \brief allowReallock Metoda říká, zda je v aktuální instanci třídy povolena realokace, či nikoliv.
     * \return True, pokud je realokace povolena, jinak false.
     */
    bool allowReallock() const;

    /*!
     * \brief delta Metoda říká po násobcích jakého čísla bude buffer v případě potřeby zvětšován. Pokud
     *              je realokace zakázána, metoda vrací hodnotu 0.
     * \return Číslo, po jehož násobcích bude buffer zvětšován, nebo 0 pokud je realokace zakázána.
     */
    int delta() const;

private:
    QVector<T> m_buffer;    //!< Dynamicky alokovaný interní buffer.
    bool m_allowReallock;   //!< Příznak povolení realokace cyklického bufferu.
    int m_delta;            //!< Po násobcích tohoto čísla se bude cyklický buffer zvětšovat při realokaci.
    int m_first;            //!< Index prvního prvku v interním bufferu.
    int m_next;             //!< Index, kam se bude zapisovat další prvek v interním bufferu.


    static constexpr int defaultBufferSize = 64;    //!< Výchozí hodnota velikosti bufferu.
    static constexpr int defaultDeltaValue = 16;    //!< Výchozí hodnota velikosti kroku při realokaci bufferu.

    /*!
     * \brief bufferSize Metoda vrátí celkovou veliksot interního bufferu.
     * \return Velikost interního bufferu.
     */
    int bufferSize();

    /*!
     * \brief moveNext Metoda posune čítač m_next o step jednotek kupředu.
     * \param step O kolik jednotek bude čítač m_next posunut.
     */
    void moveNext(int step);

    /*!
     * \brief moveFirst Metoda posune čítač m_first o step jednotek kupředu.
     * \param step O kolik jednotek bude čítač m_first posunut.
     */
    void moveFirst(int step);

    /*!
     * \brief resizeBuffer Metoda provede zvětšení bufferu o minimální možný násobek čísla m_delta tak, aby
     *                     se do bufferu vešla další data velikosti inputSize.
     * \param inputSize Počet prvků, které je třeba do bufferu zapsat.
     * \return True, pokud byl buffer úspěšně zvětšen na požadovanou velikost, jinak false.
     */
    bool resizeBufferFor(int inputSize);

    /*!
     * \brief reallockBuffer Metoda provede samotnou realokaci bufferu a případnou obnovu cykličnosti
     *                       celé datové struktury. Pokud je nově zadaná velikost větší nebo stejná jako
     *                       je ta současná, metoda vrací hodnotu false.
     * \param newSize Nová požadovaná velikost bufferu.
     * \return True, pokud realokace proběhla v pořádku, jinak false.
     */
    bool reallockBuffer(int newSize);
};

template<class T>
CyclicBuffer<T>::CyclicBuffer(int bufferSize, bool allowReallock, int delta, QObject *parent) : QObject(parent) {
    m_buffer.resize(bufferSize + 1);
    m_allowReallock = allowReallock;
    m_delta = allowReallock ? delta : 0;
    m_first = m_next = 0;
}

template<class T>
bool CyclicBuffer<T>::write(const T &item) {
    if (!resizeBufferFor(1)) // tato metoda vkládá pouze jeden prvek
        return false;

    m_buffer[m_next] = item;

    moveNext(1);
    return true;
}

template<class T>
bool CyclicBuffer<T>::write(const T *data, int size) {
    if (!resizeBufferFor(size))
        return false;

    if (m_next + size > m_buffer.size()) {
        int onEnd = m_buffer.size() - m_next;
        int onStart = size - onEnd;

        std::copy(data, data + onEnd, m_buffer.begin() + m_next);
        std::copy(data + onEnd, data + onEnd + onStart, m_buffer.begin());
    }
    else std::copy(data, data + size, m_buffer.begin() + m_next);

    moveNext(size);
    return true;
}

template<class T>
bool CyclicBuffer<T>::write(const QVector<T> &data) {
    return write(data.constData(), data.size());
}

template<class T>
bool CyclicBuffer<T>::isBuffered(int estSize) {
    return count() >= estSize;
}

template<class T>
QVector<T> CyclicBuffer<T>::take(int size) {
    return take(size, 0);
}

template<class T>
QVector<T> CyclicBuffer<T>::take(int size, int dataOverlap) {
    if (isEmpty() || size <= 0 || dataOverlap < 0 || size < dataOverlap)
        return QVector<T>();

    if (!isBuffered(size)) {
        size = count();
        dataOverlap = 0;
    }

    QVector<T> extracted;
    extracted.reserve(size);

    if (m_first + size > m_buffer.size()) {
        int onEnd = m_buffer.size() - m_first;
        int onStart = size - onEnd;

        std::copy(m_buffer.begin() + m_first, m_buffer.end(), std::back_inserter(extracted)); //TODO:
        std::copy(m_buffer.begin(), m_buffer.begin() + onStart, std::back_inserter(extracted));
    }
    else std::copy(m_buffer.begin() + m_first, m_buffer.begin() + m_first + size, std::back_inserter(extracted));

    moveFirst(size - dataOverlap);
    return extracted;
}

template<class T>
QVector<T> CyclicBuffer<T>::takeAll() {
    return take(count());
}

template<class T>
void CyclicBuffer<T>::clear() {
    m_first = m_next;

    // nebo: m_next = m_first;    ... nemůžu si vybrat :D
}

template<class T>
bool CyclicBuffer<T>::isEmpty() {
    return m_first == m_next;
}

template<class T>
int CyclicBuffer<T>::count() {
    if (m_first <= m_next) {
        // Data v bufferu "nepřetékají" z konce na začátek fyzického bufferu.
        return m_next - m_first;
    }
    else {
        // Data v bufferu "přetékají" z konce na začátek fyzického bufferu.
        return m_buffer.size() - m_first + m_next;
    }
}

template<class T>
bool CyclicBuffer<T>::allowReallock() const {
    return m_allowReallock;
}

template<class T>
int CyclicBuffer<T>::delta() const {
    return m_delta;
}

template<class T>
int CyclicBuffer<T>::bufferSize() {
    // na tomto místě a v konstruktoru je přidána uměle jednička, aby byla zachována
    // užitá aritmetika (jinak by m_first == m_next znamenalo buď prázdný nebo plný...)
    return m_buffer.size() - 1;
}

template<class T>
void CyclicBuffer<T>::moveNext(int step) {
    m_next = (m_next + step) % m_buffer.size();
}

template<class T>
void CyclicBuffer<T>::moveFirst(int step) {
    m_first = (m_first + step) % m_buffer.size();
}

template<class T>
bool CyclicBuffer<T>::resizeBufferFor(int inputSize) {
    int neededAddSpace = inputSize - bufferSize() + count();

    if (neededAddSpace <= 0)
        return true;

    int toresize = (neededAddSpace ? qCeil(neededAddSpace / static_cast<double>(m_delta)) : 1) * m_delta;
    int newSize = m_buffer.size() + toresize;

    bool realockResult = reallockBuffer(newSize);
    if (!realockResult)
        Console::writeError("CyclicBuffer: Realokace bufferu se nezdařila!");

    return realockResult;
}

template<class T>
bool CyclicBuffer<T>::reallockBuffer(int newSize) {
    if (newSize <= m_buffer.size()) {
        Console::writeError("CyclicBuffer (vnitřní chyba): Pokus o zmenšení interního bufferu!");
        return false;
    }

    if (!m_allowReallock) {
        Console::writeWarning("CyclicBuffer: Data se jiz nevejdou do bufferu a realokace za zakazana!");
        return false;
    }

    int oldSize = m_buffer.size();

    m_buffer.resize(newSize);

    if (m_first > m_next) { // tzn. že buffer "přetéká" a je třeba začátek fyzického bufferu přesunout
        QVector<T> beginItems;
        std::copy(m_buffer.begin(), m_buffer.begin() + m_next, std::back_inserter(beginItems));

        m_next = oldSize;
        return write(beginItems);
    }

    return true;
}

#endif
