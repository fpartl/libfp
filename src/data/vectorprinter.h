﻿#ifndef VECTORPRINTER_H
#define VECTORPRINTER_H

#include <QString>
#include <QTextStream>
#include <QDataStream>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QVector>
#include <QVectorIterator>

#include "../console/console.h"

/*!
 * \brief Třída Printer
 *
 * Umožňuje vypisovat vektory s položkami typu T do textových souborů pro jejich vizualizaci
 * pomocí nástrojů Octave či Matlab, na konzoli, jako raw binární data nebo textový soubor ve
 * formátu CSV. Důležité je, aby typ T existovala přetížená verze metody QString::arg().
 */
template <class T>
class VectorPrinter {

public:
    /*!
     * \brief printPlot Metoda provede výpis vektoru do textového souboru, a to jako příkaz
     *                  vytvoření grafu těchto hodnot. Pokud již soubor existuje, metoda přidá
     *                  položky vstupního vektoru na konec vypisovaných dat pokud je příkaz append
     *                  nastaven na true, jinak bude existující soubor smazán.
     * \param data Vektor, který obsahuje položky k vypsání.
     * \param fileName Název souboru, kam se budou data zapisovat.
     * \param append Pokud je nastaveno na true, budou data přídána na konec existujícího souboru,
     *               pokud ne, existující soubor bude přepsán novými daty.
     */
    static void printPlot(const QVector<T> &data, const QString &fileName, bool append = true);

    /*!
     * \brief printPlotSeries Metoda provede výpis vektoru do textového souboru, a to jako příkaz
     *                        vytvoření grafu těchto hodnot. Pokud soubor existuje, vytvoří v něm nový
     *                        příkaz vytvoření grafu a příkaz, který zaručí vizualizaci obou grafů
     *                        v jednom výstupním okně.
     * \param data Vektor, který obsahuje položky k vypsání.
     * \param fileName Název souboru, kam se budou data zapisovat.
     */
    static void printPlotSeries(const QVector<T> &data, const QString &fileName);

    /*!
     * \brief print Provede výpis položek daného vektoru do konzolového výstupu aplikace. Pokud bude výpis
     *              prováděn na jednu řádku, data budou oddělena znakem VectorPrinter::delimeter.
     * \param data Vektor, který obsahuje položky k vypsání.
     */
    static void print(const QVector<float> &data);

    /*!
     * \brief saveRawData Provede výpis položek daného vektoru do binárního souboru. Používá se například pro
     *                    následné přehrání vzorků akustického signálu nástrojem Audacity.
     * \param data Vektor, který obsahuje položky k vypsání.
     * \param fileName Název souboru, kam se budou data zapisovat.
     * \param append Pokud true, položky budou přidány na konec binárního souboru. Pokud false, soubor bude
     *               nejdříve vyprázdněn.
     */
    static void saveRawData(const QVector<T> &data, const QString &fileName, bool append);

    /*!
     * \brief printToCsv Provede výpis položek daného vektoru do textového souboru, a to jako jednu řádku
     *                   tabulkového souboru typu CSV. V případě, že výstupní soubor existuje, je do něj
     *                   přidána další řádka.
     * \param data Vektor, který obsahuje položky k vypsání.
     * \param fileName Název souboru, kam se budou data zapisovat.
     * \param append Data budou přidána na konec existujícího souboru pokud bude příznak nastaven na true,
     *               v opačném případě bude existující soubor přepsán.
     */
    static void printCsv(const QVector<T> &data, const QString &fileName, bool append = true);

    /*!
     * \brief printCsvSeries Provede výpis položek daného vektoru do csv souboru. Pokud zadaný CSV soubor
     *                       již existuje, položky budou zapsány na samostatný řádek.
     * \param data Vektor, který obsahuje položky k zápsání do CSV souboru.
     * \param fileName Název souboru, kam se budou položky vektoru zapisovat.
     */
    static void printCsvSeries(const QVector<T> &data, const QString &fileName);

private:
    static constexpr auto csvDelimeter = ";";                   //!< Oddělovač hodnot při výpisu do souboru typu CSV.
    static constexpr auto consoleDelimeter = ", ";              //!< Oddělovač hodnot při výpisu na konzoloi.
    static constexpr auto plotDelimeter = ", ";                 //!< Oddělovač hodnot při výpisu do skriptu Octave.
    static constexpr auto plotCommand = "plot([%1]);";          //!< Příkaz pro vykreslení grafu v Octave.
    static constexpr auto consoleOutput = "vektor[%1, %2](%3)"; //!< Formát výpisu vektoru do konzole.

    /*!
     * \brief vectorToString Metoda převede zadaný vektor hodnot na textový výpis, jehož hodnoty jsou odděleny
     *                       znakem delimeter.
     * \param data Vektor, jehož položky budou převedeny do textové podoby.
     * \param delimeter Oddělovač znaků, kterým budou hodnoty odděleny.
     * \return Textová podoba zadaného vektoru.
     */
    static QString vectorToString(const QVector<T> &data, const QString &delimeter);

    /*!
     * \brief checkParams Metoda zkontroluje, zda zadaný vektor obsahuje alespoň jeden prvek a cesta k souboru je
     *                    platná, tj. existuje buď konkrétní soubor nebo jeho nadřazený adresář.
     * \param data vektor, který se bude konstrolovat.
     * \param fileName Cesta k souboru, která se bude kontrolovat.
     * \return
     */
    static bool checkVariables(const QVector<T> &data, const QString &fileName);

    /*!
     * \brief openfile Metoda se pokusí otevřít zadaný soubor. Pokud soubor otevírání zdaří, metoda vrátí hodnotu true.
     *                 V opačném případě vypíše varovné hlášení a vrátí hodnotu false.
     * \param file Ukazatel na soubor, který bude otevřen.
     * \param mode Režim, ve kterém bude soubor otevřen.
     * \return True, pokud byl soubor úspěšně otevřen, jinak false.
     */
    static bool openFile(QFile * const file, QIODevice::OpenMode mode);
};

template<class T>
void VectorPrinter<T>::printPlot(const QVector<T> &data, const QString &fileName, bool append) {
    if (!VectorPrinter::checkVariables(data, fileName))
        return;

    QFile file(fileName);

    append &= file.exists();        // Nesmírně vymrdáno!

    QIODevice::OpenMode mode = append ? QFile::WriteOnly | QFile::Append
                                      : QFile::WriteOnly | QFile::Truncate;

    if (!VectorPrinter::openFile(&file, mode))
        return;

    QTextStream output(&file);

    if (append) {   // Tohle je prasána, ale nechce se mi to zbytečně zesložiťovat.
        output.seek(file.size() - 3);   // "]);" má délku tří znaků... jistě chápeš "magic" týhle konstanty.
        output << ", ";

        output << VectorPrinter::vectorToString(data, VectorPrinter::plotDelimeter);
        output << "]);";
    }
    else output << QString(VectorPrinter::plotCommand)
                        .arg(VectorPrinter::vectorToString(data, VectorPrinter::plotDelimeter));

    file.close();
}

template<class T>
void VectorPrinter<T>::printPlotSeries(const QVector<T> &data, const QString &fileName) {
    if (!VectorPrinter::checkVariables(data, fileName))
        return;

    QFile file(fileName);
    bool alreadyExists = file.exists();

    if (!VectorPrinter::openFile(&file, QFile::WriteOnly | QFile::Append))
        return;

    QTextStream output(&file);

    if (alreadyExists)
        output << "\r\n";

    output << QString(VectorPrinter::plotCommand)
                .arg(VectorPrinter::vectorToString(data, VectorPrinter::plotDelimeter));

    file.close();
}

template<class T>
void VectorPrinter<T>::print(const QVector<float> &data) {
    if (data.isEmpty())
        return;

    QString vectorMeta = QString(VectorPrinter::consoleOutput)
                            .arg(typeid(T).name())
                            .arg(data.size())
                            .arg(VectorPrinter::vectorToString(data, VectorPrinter::consoleDelimeter));

    Console::write(vectorMeta);
}

template<class T>
void VectorPrinter<T>::saveRawData(const QVector<T> &data, const QString &fileName, bool append) {
    if (!VectorPrinter::checkVariables(data, fileName))
        return;

    QFile file(fileName);

    append &= file.exists();        // Opět nesmírně vymrdáno!

    QIODevice::OpenMode mode = append
                                    ? QFile::WriteOnly | QFile::Append
                                    : QFile::WriteOnly | QFile::Truncate;

    if (!VectorPrinter::openFile(&file, mode))
        return;

    QDataStream output(&file);
    if (output.writeRawData(reinterpret_cast<const char *>(data.constData()), data.size() * sizeof(T)) < 0)
        Console::writeError(QString("Chyba pri zapisovani do raw souboru %1!").arg(fileName));

    file.close();
}

template<class T>
void VectorPrinter<T>::printCsv(const QVector<T> &data, const QString &fileName, bool append) {
    if (!VectorPrinter::checkVariables(data, fileName))
        return;

    QFile file(fileName);

    append &= file.exists();        // Opět nesmírně vymrdáno!

    QIODevice::OpenMode mode = append
                                    ? QFile::WriteOnly | QFile::Append
                                    : QFile::WriteOnly | QFile::Truncate;

    if (!VectorPrinter::openFile(&file, mode))
        return;

    QTextStream output(&file);

    if (append)
        output << VectorPrinter::csvDelimeter;

    output << VectorPrinter::vectorToString(data, VectorPrinter::csvDelimeter);

    file.close();
}

template<class T>
void VectorPrinter<T>::printCsvSeries(const QVector<T> &data, const QString &fileName) {
    if (!VectorPrinter::checkVariables(data, fileName))
        return;

    QFile file(fileName);
    bool alreadyExists = file.exists();

    if (!VectorPrinter::openFile(&file, QFile::WriteOnly | QFile::Append))
        return;

    QDataStream output(&file);
    if (alreadyExists)
        output << "\r\n";

    output << VectorPrinter::vectorToString(data, VectorPrinter::csvDelimeter);

    file.close();
}

template<class T>
QString VectorPrinter<T>::vectorToString(const QVector<T> &data, const QString &delimeter) {
    if (data.isEmpty() || delimeter.isEmpty())
        return QString();

    QString result;

    QVectorIterator<T> i(data);
    while (i.hasNext()) {
        result.append(QString("%1").arg(i.next()));

        if (i.hasNext())
            result.append(delimeter);
    }

    return result;
}

template<class T>
bool VectorPrinter<T>::checkVariables(const QVector<T> &data, const QString &fileName) {
    if (data.isEmpty())
        return false;

    if (!QFileInfo(fileName).dir().exists()) {
        Console::writeError(QString("Adresar %1 neexistuje!").arg(QFileInfo(fileName).dir().path()));
        return false;
    }

    return true;
}

template<class T>
bool VectorPrinter<T>::openFile(QFile * const file, QIODevice::OpenMode mode) {
    if (!file)
        return false;

    if (file->open(mode)) {
        return true;
    }
    else {
        Console::writeError(QString("Do souboru %1 bohuzel nelze zapisovat.").arg(file->fileName()));
        return false;
    }
}

#endif


