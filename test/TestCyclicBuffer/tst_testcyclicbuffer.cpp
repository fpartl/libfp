#include <QtTest>
#include <QVector>
#include <QRandomGenerator>
#include <QString>

#include "../../src/structures/cyclicbuffer.h"
#include "../../src/console/console.h"
#include "../../src/console/console.cpp"
#include "../../src/console/textstyle.h"
#include "../../src/console/textstyle.cpp"

/* Užitá počáteční velikost. */
#define BUFFER_SIZE 4

/* Užitá delta při realokaci bufferu. */
#define BUFFER_DELTA 2

/* Datový typ položek testovaného bufferu. */
using buff_item_t = int;

/* Maximální velikosti generovaných testovacích vektorů. */
#define MAX_TEST_VECTOR_SIZE 128

/* Počet scénářů při každém testu. */
#define SCENARIOS_COUNT 4096

/*!
 * \brief Třída TestCyclicBuffer
 *
 * Unit test pro třídu CyclicBuffer.
 */
class TestCyclicBuffer : public QObject {
    Q_OBJECT

private slots:
    /*!
     * \brief testClear Metoda otestuje metody count(), isEmpty() a isBuffered() při vyprázdnění bufferu.
     */
    void testClearing();

    /*!
     * \brief testClearing_data Metoda poskytující data pro testovací metodu testClearing().
     */
    void testClearing_data();

    /*!
     * \brief testCount Metoda otestuje metody count(), isEmpty() a isBuffered() při zaplnění bufferu.
     */
    void testCounting();

    /*!
     * \brief testCount_data Metoda poskytující data pro testovací metodu testCounting().
     */
    void testCounting_data();

    /*!
     * \brief testWriteOne Metoda otestuje metody write() pro jeden prvek, isBuffered(), take() a takeAll().
     */
    void testWriteTakeOne();

    /*!
     * \brief testWriteTakeOne_data Metoda poskytující data pro testovací metodu testWriteTakeOne().
     */
    void testWriteTakeOne_data();

    /*!
     * \brief testWriteTake Metoda otestuje metody write() pro více prvků, take() a takeAll().
     */
    void testWriteTake();

    /*!
     * \brief testWriteTake_data Metoda poskytující data pro testovací metodu testWriteTake().
     */
    void testWriteTake_data();

    /*!
     * \brief testTakeOverlap Metoda zkontroluje správnost překrývání se dat.
     */
    void testTakeOverlap();

    /*!
     * \brief testTakeOverlap_data Metoda poskytující data pro testovací metodu testTakeOverlap().
     */
    void testTakeOverlap_data();

private:
    QRandomGenerator m_generator = QRandomGenerator::securelySeeded();    //!< Generátor náhodných čísel.

    /*!
     * \brief generateVector Metoda vygeneruje vektor s náhodnými hodnotami o délce size. Pokud je size nastaven
     *                       na nula nebo hodnotě menší, délka vektoru bude taktéž náhodná.
     * \param size Počet prvků ve výsledném vektoru.
     * \return Vygenerovaný vektor délky size nebo náhodné pokud je size <= 0.
     */
    QVector<buff_item_t> generateVector(int size = 0);
};

void TestCyclicBuffer::testClearing() {
    CyclicBuffer<buff_item_t> buffer(BUFFER_SIZE, true, BUFFER_DELTA);

    QFETCH(QVector<buff_item_t>, input);

    buffer.write(input);

    buffer.clear();
    QVERIFY2(buffer.isEmpty(), "Po vyprázdnění bufferu metodou clear() v něm zřejmě ještě něco zbylo.");
    QVERIFY2(buffer.count() == 0, "Počet prvků není po volání metody clear() nulový.");
    QVERIFY2(buffer.isBuffered(0), "Po vyprázdnění bufferu v něm není připraveno nula prvků.");
    QVERIFY2(!buffer.isBuffered(1), "Po vyprázdnění bufferu je v něm minimálně jeden prvek připraven.");
}

void TestCyclicBuffer::testClearing_data() {
    QTest::addColumn<QVector<buff_item_t>>("input");

    for (int i = 0; i < SCENARIOS_COUNT; i++)
        QTest::newRow(QString("clearing %1").arg(i).toLocal8Bit().constData()) << generateVector();
}

void TestCyclicBuffer::testCounting() {
    CyclicBuffer<buff_item_t> buffer(BUFFER_SIZE, true, BUFFER_DELTA);

    QFETCH(QVector<buff_item_t>, input);

    buffer.write(input);
    QVERIFY2(buffer.count() == input.size(), QString("Po zapsání %1 prvků je počet prvků v bufferu %2.").arg(input.size()).arg(buffer.count()).toLocal8Bit().data());
    QVERIFY2(buffer.isBuffered(input.size()), QString("Po zapsání %1 prvků tyto prvky nejsou k dispozici.").arg(input.size()).toLocal8Bit().data());
    QVERIFY2(!buffer.isBuffered(input.size() + 1), "V bufferu je k dispozici více prvků než do něj bylo zapsáno.");
    QVERIFY2(!buffer.isEmpty(), "Po zapsání do bufferu se buffer jeví jako prázdný.");

    buffer.clear();
    buffer.write(input.constData(), input.size());
    QVERIFY2(buffer.count() == input.size(), QString("Po zapsání %1 prvků je počet prvků v bufferu %2.").arg(input.size()).arg(buffer.count()).toLocal8Bit().data());
    QVERIFY2(buffer.isBuffered(input.size()), QString("Po zapsání %1 prvků tyto prvky nejsou k dispozici.").arg(input.size()).toLocal8Bit().data());
    QVERIFY2(!buffer.isBuffered(input.size() + 1), "V bufferu je k dispozici více prvků než do něj bylo zapsáno.");
    QVERIFY2(!buffer.isEmpty(), "Po zapsání do bufferu se buffer jeví jako prázdný.");
}

void TestCyclicBuffer::testCounting_data() {
    QTest::addColumn<QVector<buff_item_t>>("input");

    for (int i = 0; i < SCENARIOS_COUNT; i++)
        QTest::newRow(QString("counting %1").arg(i).toLocal8Bit().constData()) << generateVector();
}

void TestCyclicBuffer::testWriteTakeOne() {
    CyclicBuffer<buff_item_t> buffer(BUFFER_SIZE, true, BUFFER_DELTA);

    QFETCH(buff_item_t, input);

    buffer.write(input);
    QVERIFY2(buffer.isBuffered(1), "Po vložení jednoho prvku do bufferu tento prvek není k dispozici.");
    QVERIFY2(!buffer.isBuffered(1 + 1), "Po vložení jednoho prvku do bufferu v něm je k dispozici více hodnot.");
    QCOMPARE(buffer.takeAll(), QVector<buff_item_t>({input}));

    buffer.write(input);
    QCOMPARE(buffer.take(0), QVector<buff_item_t>());
    QCOMPARE(buffer.take(1), QVector<buff_item_t>({input}));

    buffer.write(input);
    QCOMPARE(buffer.take(2), QVector<buff_item_t>({input}));
}

void TestCyclicBuffer::testWriteTakeOne_data() {
    QTest::addColumn<buff_item_t>("input");

    for (int i = 0; i < SCENARIOS_COUNT; i++)
        QTest::newRow(QString("writeTakeOne %1").arg(i).toLocal8Bit().constData()) << static_cast<buff_item_t>(m_generator.generate());
}

void TestCyclicBuffer::testWriteTake() {
    CyclicBuffer<buff_item_t> buffer(BUFFER_SIZE, true, BUFFER_DELTA);

    QFETCH(QVector<buff_item_t>, input);

    buffer.write(input);
    QCOMPARE(buffer.takeAll(), input);

    buffer.write(input.constData(), input.size());
    QCOMPARE(buffer.takeAll(), input);

    int subsetSize = m_generator.bounded(input.size());

    buffer.write(input);
    QCOMPARE(buffer.take(subsetSize), input.mid(0, subsetSize));

    buffer.clear();

    buffer.write(input.constData(), input.size());
    QCOMPARE(buffer.take(subsetSize), input.mid(0, subsetSize));
}

void TestCyclicBuffer::testWriteTake_data() {
    QTest::addColumn<QVector<buff_item_t>>("input");

    for (int i = 0; i < SCENARIOS_COUNT; i++)
        QTest::newRow(QString("writeTake %1").arg(i).toLocal8Bit().constData()) << generateVector();
}

void TestCyclicBuffer::testTakeOverlap() {
    CyclicBuffer<buff_item_t> buffer(BUFFER_SIZE, true, BUFFER_DELTA);

    QFETCH(QVector<buff_item_t>, input);
    int first = 0;

    buffer.write(input);
    while (!buffer.isEmpty()) {
        int size = m_generator.bounded(input.size() - 1) + 1; // -1 +1, aby byl vektor vždy alespoň jednoprvkový
        int overlap = m_generator.bounded(size);

        QVector<buff_item_t> out = buffer.take(size, overlap);

        QVERIFY2(out.size() <= size, QString("Vybralo se %1 prvků, když jsem chtěl jenom %2.").arg(out.size()).arg(size).toLocal8Bit().data());
        QVERIFY2(size == 0 || !out.isEmpty(), QString("Vrácený vektor je prázdný i když by velikost měla být %1.").arg(size).toLocal8Bit().data());
        QVERIFY2(out.size() == size || buffer.isEmpty(), QString("Mind fuck nevyšel.").toLocal8Bit().data());
        QCOMPARE(out, input.mid(first, out.size()));

        first += out.size() - overlap;
    }
}

void TestCyclicBuffer::testTakeOverlap_data() {
    QTest::addColumn<QVector<buff_item_t>>("input");

    for (int i = 0; i < SCENARIOS_COUNT; i++)
        QTest::newRow(QString("testTakeOverlap %1").arg(i).toLocal8Bit().constData()) << generateVector();
}

QVector<buff_item_t> TestCyclicBuffer::generateVector(int size) {
    if (size <= 0)
        size = m_generator.bounded(MAX_TEST_VECTOR_SIZE - 1) + 1; // -1 +1, aby byl vektor vždy alespoň jednoprvkový

    QVector<buff_item_t> vector;
    vector.resize(size);

    Q_ASSERT(vector.size() == size);

    for (int i = 0; i < vector.size(); i++)
        vector[i] = static_cast<buff_item_t>(m_generator.generate());

    return vector;
}

QTEST_APPLESS_MAIN(TestCyclicBuffer)
#include "tst_testcyclicbuffer.moc"
