#include <QtTest>
#include <QVector>
#include <QRandomGenerator>
#include <QString>

#include "../../src/console/textstyle.h"
#include "../../src/console/textstyle.cpp"

/* Maximální velikosti generovaných testovacích řetězců. */
#define MAX_TEST_STRING_SIZE 128

/* Počet scénářů při každém testu. */
#define SCENARIOS_COUNT 4096

/*!
 * \brief Třída TestTextStyle
 *
 * Unit test pro třídu TextStyle (pouze její uchovávací vlastnosti -> konstrutor + gettery).
 * Pro kontrolu generování stylovaných textu spustně projekt TestConsole.
 */
class TestTextStyle : public QObject {
    Q_OBJECT

private slots:
    /*!
     * \brief testConstructorsGetters Metoda zkontroluje, zda třídě zprávně funguje konstruktory a gettery.
     */
    void testConstructorsGetters();

    /*!
     * \brief testConstructorsGetters_data Metoda poskytující data pro testovací metodu testConstructorsGetters().
     */
    void testConstructorsGetters_data();

private:
    QRandomGenerator m_generator = QRandomGenerator::securelySeeded();    //!< Generátor náhodných čísel.

    /*!
     * \brief generateString Metoda vygeneruje náhodný řetězec znaků délky size. Pokud je size <= 0,
     *                       vygenerovaný řetězec bude mít délku náhodnou z rozsahu <1, MAX_TEST_STRING_SIZE)
     * \param size Délka generovaného řetězce (pokud <= 0 viz popis metody).
     * \return Vygenerovaný řetězec.
     */
    QString generateString(int size = 0);

    /*!
     * \brief generateForeground Metoda vygeneruje náhodný kód barvy podle enumerického typu ForegroudColors.
     * \return Náhodný kód barvy popředí textu.
     */
    TextStyle::ForegroudColors generateForeground();

    /*!
     * \brief generateBackground Metoda vygeneruje náhodný kód barvy pozadí podle enumerického typu BackgroudColors.
     * \return Náhodný kód barvy pozadí textu.
     */
    TextStyle::BackgroudColors generateBackground();

    /*!
     * \brief generateDecorations Metoda vygeneruje vektor dekorací. Podle enumerického typu Decorations tedy bude
     *                            obsahovat maximálně 3 položky a minimálně 1.
     * \return Vektor vygenerovaných dekorací textu.
     */
    QVector<TextStyle::Decorations> generateDecorations();
};

void TestTextStyle::testConstructorsGetters() {
    QFETCH(TextStyle::ForegroudColors, foreground);
    QFETCH(TextStyle::BackgroudColors, background);
    QFETCH(QVector<TextStyle::Decorations>, decorations);

    TextStyle style(foreground, background, decorations);

    QCOMPARE(style.foreground(), foreground);
    QCOMPARE(style.background(), background);
    QCOMPARE(style.decorations(), decorations);
}

void TestTextStyle::testConstructorsGetters_data() {
    QTest::addColumn<TextStyle::ForegroudColors>("foreground");
    QTest::addColumn<TextStyle::BackgroudColors>("background");
    QTest::addColumn<QVector<TextStyle::Decorations>>("decorations");

    for (int i = 0; i < SCENARIOS_COUNT; i++) {
        QTest::newRow(QString("constructing %1").arg(i).toLocal8Bit().constData())
            << generateForeground()
            << generateBackground()
            << generateDecorations();
    }
}

QString TestTextStyle::generateString(int size) {
    if (size <= 0)
        size = m_generator.bounded(MAX_TEST_STRING_SIZE - 1) + 1; // -1 +1, aby měl řetězec vždy nenulovou délku.

    const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");

    QString randomString;
    for (int i = 0; i < size; i++) {
        int randomIndex = m_generator.bounded(possibleCharacters.length());
        QChar randomCharacter = possibleCharacters.at(randomIndex);

        randomString.append(randomCharacter);
    }

    return randomString;
}

TextStyle::ForegroudColors TestTextStyle::generateForeground() {
    QVector<TextStyle::ForegroudColors> foregrounds;

    foregrounds
        << TextStyle::DefaultFore
        << TextStyle::BlackFore
        << TextStyle::RedFore
        << TextStyle::GreenFore
        << TextStyle::YellowFore
        << TextStyle::BlueFore
        << TextStyle::MagentaFore
        << TextStyle::CyanFore
        << TextStyle::WhiteFore;

    int randomIndex = m_generator.bounded(foregrounds.size());

    return foregrounds.at(randomIndex);
}

TextStyle::BackgroudColors TestTextStyle::generateBackground() {
    QVector<TextStyle::BackgroudColors> backgrounds;

    backgrounds
        << TextStyle::DefaultBack
        << TextStyle::BlackBack
        << TextStyle::RedBack
        << TextStyle::GreenBack
        << TextStyle::YellowBack
        << TextStyle::BlueBack
        << TextStyle::MagentaBack
        << TextStyle::CyanBack
        << TextStyle::WhiteBack;

    int randomIndex = m_generator.bounded(backgrounds.size());

    return backgrounds.at(randomIndex);
}

QVector<TextStyle::Decorations> TestTextStyle::generateDecorations() {
    QVector<TextStyle::Decorations> decorations;

    decorations
        << TextStyle::Bold
        << TextStyle::Underline
        << TextStyle::Inverse;

    /* -1 +1, aby bylo zaručeno, aby tam vždy alespoň jedna dekorace byla. */
    int count = m_generator.bounded(decorations.size() - 1) + 1;
    QVector<TextStyle::Decorations> generated;

    for (int i = 0; i < count; i++)
        generated << decorations.at(i);

    return generated;
}

QTEST_APPLESS_MAIN(TestTextStyle)
#include "tst_testtextstyle.moc"
