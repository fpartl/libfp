TEMPLATE = subdirs

SUBDIRS += \
    TestConsole \
    TestCyclicBuffer \
    TestStatistics \
    TestTextStyle
