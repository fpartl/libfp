#include <QtTest>
#include <QtMath>

#include "../../src/math/statistics.h"

/*!
 * \brief Třída TestStatistics
 *
 * Obsahuje metody pro testování třídy Statistics. Nejedná se o data driven testování. Hodnoty jsem si
 * sám spočítal vedle na papír a v kdo jsou uvedeny jako magic konstanty.
 */
class TestStatistics : public QObject {
    Q_OBJECT

private slots:
    /*!
     * \brief testMax Metoda zkontroluje metodu Statistics<T>::arithmeticMean(const QVector<T> &set).
     */
    void testMax();

    /*!
     * \brief testIndexOfMax Metoda zkontroluje metodu Statistics<T>::arithmeticMean(const QVector<T> &set).
     */
    void testIndexOfMax();

    /*!
     * \brief testMin Metoda zkontroluje metodu Statistics<T>::arithmeticMean(const QVector<T> &set).
     */
    void testMin();

    /*!
     * \brief testIndexOfMin Metoda zkontroluje metodu Statistics<T>::arithmeticMean(const QVector<T> &set).
     */
    void testIndexOfMin();

    /*!
     * \brief testStandardDeviation Metoda zkontroluje metodu Statistics<T>::arithmeticMean(const QVector<T> &set).
     */
    void testStandardDeviation();

    /*!
     * \brief testModus Metoda zkontroluje metodu Statistics<T>::arithmeticMean(const QVector<T> &set).
     */
    void testModus();

    /*!
     * \brief testMedian Metoda zkontroluje metodu Statistics<T>::arithmeticMean(const QVector<T> &set).
     */
    void testMedian();

    /*!
     * \brief testVariance Metoda zkontroluje metodu Statistics<T>::arithmeticMean(const QVector<T> &set).
     */
    void testVariance();

    /*!
     * \brief testArithMean Metoda zkontroluje metodu Statistics<T>::arithmeticMean(const QVector<T> &set).
     */
    void testArithMean();
};

void TestStatistics::testMax() {
    QVector<int> set = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    QCOMPARE(Statistics::max<int>(set), 10);
}

void TestStatistics::testIndexOfMax() {
    QVector<int> set = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    QCOMPARE(Statistics::indexOfMax<int>(set), 9);
}

void TestStatistics::testMin() {
    QVector<int> set = {10, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    QCOMPARE(Statistics::min<int>(set), 1);
}

void TestStatistics::testIndexOfMin() {
    QVector<int> set = {10, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    QCOMPARE(Statistics::indexOfMin<int>(set), 2);
}

void TestStatistics::testStandardDeviation() {
    QVector<int> set = {1, 2, 3, 4, 5, 6, 7};

    QCOMPARE(Statistics::standardDeviation<int>(set), 2.0);
    QCOMPARE(sqrt(Statistics::variance<int>(set)), Statistics::standardDeviation<int>(set));
}

void TestStatistics::testModus() {
    QVector<int> set = {1, 2, 3, 4, 5, 6, 7, 1, 1};

    QCOMPARE(Statistics::modus<int>(set), 1);
}

void TestStatistics::testMedian() {
    QVector<int> set1 = {1, 2, 3, 4, 5, 6, 7};
    QVector<int> set2 = {1, 2, 3, 4, 5, 6, 7, 8};

    QCOMPARE(Statistics::median(set1), 4.0);
    QCOMPARE(Statistics::median(set2), 4.5);
}

void TestStatistics::testVariance() {
    QVector<int> set = {1, 2, 3, 4, 5, 6, 7};

    QCOMPARE(Statistics::variance<int>(set), 4.0);
}

void TestStatistics::testArithMean() {
    QVector<int> set = {1, 2, 3, 4, 5, 6, 7};

    QCOMPARE(Statistics::arithmeticMean<int>(set), 4.0);
}



QTEST_APPLESS_MAIN(TestStatistics)
#include "tst_teststatistics.moc"
