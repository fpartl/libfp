#include <QCoreApplication>
#include <QDebug>

#include "../../src/console/console.h"
#include "../../src/console/console.cpp"
#include "../../src/console/textstyle.h"
#include "../../src/console/textstyle.cpp"

int main(int argc, char *argv[]) {
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    Console::write("Nestylovaný text.");
    Console::write("Další nestylovaný text na stejné řádce.");
    Console::writeLine();
    Console::writeLine();
    Console::writeLine("Po jedné prázdné řádce bude text nestylovaný na samostatné řádce.");
    Console::writeLine("A další nestylovaný text na samostatné řádce.");
    Console::writeLine();

    Console::writeError("Tohle by mělo vypadat jako chyba.");
    Console::writeSuccess("Tohle by mělo vypadat jako úspěch.");
    Console::writeWarning("Tohle by mělo vypadat jako varování.");
    Console::writeNotice("Tohle by mělo vypadat jako upomínka.");

    Console::writeLine("Zelený tučný a podtržený text.", TextStyle(TextStyle::GreenFore, TextStyle::DefaultBack, QVector<TextStyle::Decorations>({ TextStyle::Bold, TextStyle::Underline })));
    Console::writeLine("Červený tučný a podtržený text na bílém pozadí.", TextStyle(TextStyle::RedFore, TextStyle::WhiteBack, QVector<TextStyle::Decorations>({ TextStyle::Bold, TextStyle::Underline })));
    Console::writeLine("A otočené barvy...", TextStyle(TextStyle::RedFore, TextStyle::WhiteBack, QVector<TextStyle::Decorations>({ TextStyle::Bold, TextStyle::Underline, TextStyle::Inverse })));
    Console::writeLine("Černý text na bílém pozadí.", TextStyle(TextStyle::BlackFore, TextStyle::WhiteBack));
    Console::writeLine("Světle modrý text na modrém pozadí.", TextStyle(TextStyle::CyanFore, TextStyle::BlueBack));
    Console::writeLine("Tučný fialový text na žlutém pozadí... a to by snad mělo stačit.",
        TextStyle(TextStyle::MagentaFore, TextStyle::YellowBack, QVector<TextStyle::Decorations>({ TextStyle::Bold })));

    if (Console::writeQuestion("Vypsalo se všechno dobře? (otázka je modře na bílém pozadí)", TextStyle(TextStyle::BlueFore, TextStyle::WhiteBack))) {
        Console::writeLine("Odpověděli jste, že ano... to je výborné! (bílé na fialovém pozadí a podtržené).",
            TextStyle(TextStyle::WhiteFore, TextStyle::MagentaBack, QVector<TextStyle::Decorations>({ TextStyle::Underline })));

        if (Console::writeConfirm("Tak já teda jdu, ok? (fialové na zeleném pozadí a tučně)",
                TextStyle(TextStyle::MagentaFore, TextStyle::GreenBack, QVector<TextStyle::Decorations>({ TextStyle::Bold })))) {
            Console::writeDebugInfo("Tak já pádím, tak nazdar! A navíc tučně!", true);
        }
        else Console::writeDebugInfo("Stejně jdu! A klidně i netučně!", false);

        return EXIT_SUCCESS;
    }
    else {
        Console::writeError("Zdá se, že se teda něco pokakalo...");

        return EXIT_FAILURE;
    }
}
